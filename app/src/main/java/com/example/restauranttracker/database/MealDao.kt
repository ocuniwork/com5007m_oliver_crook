package com.example.restauranttracker.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MealDao {
    @Query("SELECT * FROM meals")
    fun getAll(): List<MealEntity>

    @Query("SELECT * FROM meals WHERE mealRestaurantID = :id")
    fun getRestaurantMeals(id : Int): List<MealEntity>

    @Query("SELECT * FROM meals WHERE mealName = :mealname")
    fun getMeal(mealname : String): List<MealEntity>

    @Query("SELECT * FROM meals WHERE mealID = :mealid")
    fun getMealByID(mealid : Int): List<MealEntity>


    @Insert
    fun insertAll(vararg  meal: MealEntity)
}