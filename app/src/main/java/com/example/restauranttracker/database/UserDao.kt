package com.example.restauranttracker.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface UserDao {
    @Query("SELECT * FROM users")
    fun getAll(): List<UserEntity>

    @Query("SELECT * FROM users WHERE username = :username")
    fun getUser(username: String): List<UserEntity>

    @Query("SELECT * FROM users WHERE userID = :userid")
    fun getUserByID(userid: Int): List<UserEntity>

    @Insert
    fun insertAll(vararg  user: UserEntity)
}