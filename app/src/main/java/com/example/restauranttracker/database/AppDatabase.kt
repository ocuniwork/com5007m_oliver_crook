package com.example.restauranttracker.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import java.math.BigInteger
import java.security.MessageDigest

@Database(entities = [RestaurantEntity::class, MealEntity::class, OrderEntity::class, UserEntity::class, SessionEntity::class], version = 18)
abstract class AppDatabase : RoomDatabase() {
    abstract fun restaurantDao(): RestaurantDao
    abstract fun mealDao(): MealDao
    abstract fun orderDao(): OrderDao
    abstract fun userDao(): UserDao
    abstract fun sessionDao(): SessionDao
}