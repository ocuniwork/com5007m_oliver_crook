package com.example.restauranttracker.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "meals")
data class MealEntity (
    @PrimaryKey(autoGenerate = true) val mealID : Int?,
    @ColumnInfo val mealName : String,
    @ColumnInfo val mealPrice : String,
    @ColumnInfo val mealRestaurantID : Int
)