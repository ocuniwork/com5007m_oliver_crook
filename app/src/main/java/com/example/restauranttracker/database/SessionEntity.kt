package com.example.restauranttracker.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "session")
data class SessionEntity (
    @PrimaryKey(autoGenerate = true) val sessionID : Int?,
    @ColumnInfo val username : String?,
    @ColumnInfo val remembered : Int?,
    @ColumnInfo val restaurant : Int?
)