package com.example.restauranttracker.database

import androidx.room.Dao
import androidx.room.Index
import androidx.room.Insert
import androidx.room.Query

@Dao
interface OrderDao {
    @Query("SELECT * FROM orders")
    fun getAll(): List<OrderEntity>

    @Query("SELECT * FROM orders ORDER BY date(orderDate) ASC ")
    fun getAllByDate(): List<OrderEntity>

    @Insert
    fun insertAll(vararg  order: OrderEntity)
}