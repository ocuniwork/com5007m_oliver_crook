package com.example.restauranttracker.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface SessionDao {
    @Query("SELECT * FROM session ")
    fun getSession(): List<SessionEntity>

    @Query("UPDATE session SET username = :username, remembered = :remembered WHERE sessionID = 1")
    fun updateSession(username : String, remembered : Boolean)

    @Query("UPDATE session SET restaurant = :restaurant WHERE sessionID = 1")
    fun setRestaurant(restaurant : Int)

    @Insert
    fun addSession(vararg  session: SessionEntity)
}