package com.example.restauranttracker.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "restaurants")
data class RestaurantEntity (
    @PrimaryKey(autoGenerate = true) val restaurantID : Int?,
    @ColumnInfo val restaurantName : String,
    @ColumnInfo val restaurantAddress : String,
    @ColumnInfo val restaurantDescription : String
)