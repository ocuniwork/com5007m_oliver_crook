package com.example.restauranttracker.database

import androidx.room.*

@Entity(tableName = "orders")
data class OrderEntity (
    @PrimaryKey(autoGenerate = true) val orderID : Int?,
    @ColumnInfo val orderUserID : Int,
    @ColumnInfo val orderMealID : Int,
    @ColumnInfo val orderFlavourScore : String,
    @ColumnInfo val orderPriceScore : String,
    @ColumnInfo val orderDate : String
)