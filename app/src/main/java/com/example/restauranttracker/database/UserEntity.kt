package com.example.restauranttracker.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class UserEntity (
    @PrimaryKey(autoGenerate = true) val userID : Int?,
    @ColumnInfo val username : String,
    @ColumnInfo val password : String
)