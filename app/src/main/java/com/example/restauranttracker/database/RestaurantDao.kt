package com.example.restauranttracker.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface RestaurantDao {
    @Query("SELECT * FROM restaurants")
    fun getAll(): List<RestaurantEntity>

    @Query("SELECT * FROM restaurants WHERE restaurantName = :name")
    fun getRestaurantByName(name : String): List<RestaurantEntity>

    @Query("SELECT * FROM restaurants WHERE restaurantID = :restaurantid")
    fun getRestaurantByID(restaurantid : Int): List<RestaurantEntity>

    @Insert
    fun insertAll(vararg restaurant: RestaurantEntity)

    @Query("DELETE FROM restaurants")
    fun deleteAll()
}