package com.example.restauranttracker

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.room.Room
import com.example.restauranttracker.database.AppDatabase

class LoginPage : AppCompatActivity() {

    lateinit var instructions : TextView
    lateinit var username : EditText
    lateinit var password : EditText
    lateinit var remember : CheckBox
    lateinit var login : Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_page)

        // Get database
        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "restaurantdatabase"
        ).allowMainThreadQueries().build()

        // Setting up views
        instructions = findViewById(R.id.textLoginInsructions)
        username = findViewById(R.id.editTextUsername)
        password = findViewById(R.id.editTextPassword)
        remember = findViewById(R.id.rememberMe)
        login    = findViewById(R.id.buttonLogin)

        // If the name string passed from intent does not equal "non",
        // then populate username and password edittexts with username and password
        if (!intent.getStringExtra("name").equals("non")){
            username.setText(intent.getStringExtra("name"))
            password.setText(intent.getStringExtra("pass"))

        }

        // When Log In is clicked ...
        login.setOnClickListener {
            var u = username.text
            var user = db.userDao().getUser(u.toString())

            // ... first check if username is NOT in database ...
            if (user.size == 0){
                instructions.text = "Username not found"
            }

            // ... if username is found then proceed to password check.
            else {

                // If password, when hashed, matches the hashed password on the database ...
                if (user[0].password.equals(md5(password.text.toString()))){

                    // ... then log in and remember user if 'remember' checkbox is ticked ...
                    if (remember.isChecked) {
                        db.sessionDao().updateSession(u.toString(), true)
                        val intent = Intent(this@LoginPage,MainMenu::class.java)
                        startActivity(intent)
                    }
                    // ... or log in and don't remember them if 'remember' checkbox is not ticked.
                    else{
                        db.sessionDao().updateSession(u.toString(), false)
                        val intent = Intent(this@LoginPage,MainMenu::class.java)
                        startActivity(intent)
                    }
                }
            }
        }
    }

}