package com.example.restauranttracker

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.restauranttracker.database.AppDatabase
import com.example.restauranttracker.database.OrderEntity

class Meals : AppCompatActivity() {

    lateinit var rv : RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meals)

        // Get database
        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "restaurantdatabase"
        ).allowMainThreadQueries().build()

        // Get a list of all orders (by date)
        var orders = db.orderDao().getAllByDate()

        // Set up recyclerview and adapter, attach adapter to recyclerview.
        rv = findViewById(R.id.recyclerMeals)
        rv.layoutManager = LinearLayoutManager(this)
        var adapter = OrderAdapter(this, orders as ArrayList<OrderEntity>)
        rv.adapter = adapter
    }
}