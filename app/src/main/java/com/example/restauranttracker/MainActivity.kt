package com.example.restauranttracker

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.room.Room
import com.example.restauranttracker.database.AppDatabase
import com.example.restauranttracker.database.MealEntity
import com.example.restauranttracker.database.RestaurantEntity
import com.example.restauranttracker.database.SessionEntity
import java.math.BigInteger
import java.security.MessageDigest

class MainActivity : AppCompatActivity() {



    lateinit var navLogin : Button
    lateinit var navRegister: Button
    lateinit var debugger : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Get database
        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "restaurantdatabase"
        ).allowMainThreadQueries().fallbackToDestructiveMigration().build()

        // Run function to check if database needs initialising,
        // function also initialises database if needed
        initDatabase(db)

        // If the username for the session is not "nan", then take the user to the Main Menu page
        if (!db.sessionDao().getSession()[0].username.equals("nan")){
            val intent = Intent(this@MainActivity, MainMenu::class.java)
            startActivity(intent)
        }

        // Setting up views
        navLogin = findViewById(R.id.buttonNavLogin)
        navRegister = findViewById(R.id.buttonNavRegister)

        // If the Log In button is clicked, take the user to the Log In page
        navLogin.setOnClickListener {
            val intent = Intent(this@MainActivity,LoginPage::class.java)
            intent.putExtra("name", "non")
            startActivity(intent)
        }

        // If the Register button is clicked, take the user to the Register page
        navRegister.setOnClickListener {
            val intent = Intent(this@MainActivity,Register::class.java)
            startActivity(intent)
        }
    }

    // Overrding the pause function
    // When pausing, check if user is to be remembered.
    // If they are not to be remembered, then set session to default values
    override fun onPause(){
        super.onPause()
        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "restaurantdatabase"
        ).allowMainThreadQueries().fallbackToDestructiveMigration().build()

        val session = db.sessionDao().getSession()[0]
        if (session.remembered == 0){
            db.sessionDao().updateSession("nan", false)
        }
    }

    // Function that initialises the database if required
    fun initDatabase(db : AppDatabase) {

        // If the total number of restaurants is fewer than 5 (the app either has 0 or 10)*,
        // then create a Session entry, 10 Restaurant entries, and 14 Meal entries for each Restaurant.
        // *if 10 then database has been set up, if 0 then database has to be set up
        if (db.restaurantDao().getAll().size < 5){
            db.sessionDao().addSession(SessionEntity(null, "nan", null, 1))

            db.restaurantDao().deleteAll()
            db.restaurantDao().insertAll(RestaurantEntity(null, "Bill's Restaurant", "12 Coney Street", "Bill's Restaurant is a high end restaurant found in York, and it's known for its relaxing atmosphere"))
            db.restaurantDao().insertAll(RestaurantEntity(null, "YUZU Street Food", "Unit 6, Enterprise Complex", "A hip and groovy Asian-fusion eatery, owned by Mr. Street Food"))
            db.restaurantDao().insertAll(RestaurantEntity(null, "Cosy Club", "19-22 Fossgate", "A club that is cozy"))
            db.restaurantDao().insertAll(RestaurantEntity(null, "Prezzo Italian", "1 Clifford Street", "Italian food restaurant"))
            db.restaurantDao().insertAll(RestaurantEntity(null, "31 Castlegate", "31 Castlegate", "I wouldn't trust a restaurant that's got an address for a name"))
            db.restaurantDao().insertAll(RestaurantEntity(null, "Il Paradiso Del Cibo", "40 Walmgate", "asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf asdf "))
            db.restaurantDao().insertAll(RestaurantEntity(null, "Ambiente Tapas", "31 Fossgate", "Ambiente Tapas has lots of Ambiente Tapas. It's relaxing"))
            db.restaurantDao().insertAll(RestaurantEntity(null, "Byron", "11 High Ousegate", "Good burgers"))
            db.restaurantDao().insertAll(RestaurantEntity(null, "Burgsy's", "9 Castlegate", "Restaurant found in York"))
            db.restaurantDao().insertAll(RestaurantEntity(null, "Rustique", "28 Castlegate", "asdfagasdfasdfagasdfasdfagasdfasdfagasdfasdfagasdf\nasdfasdfasdf"))

            db.mealDao().insertAll(MealEntity(null, "Meal TCC0E", "£21",1))
            db.mealDao().insertAll(MealEntity(null, "Meal OM6MS", "£10",1))
            db.mealDao().insertAll(MealEntity(null, "Meal 9F550", "£2",1))
            db.mealDao().insertAll(MealEntity(null, "Meal SZWH6", "£7",1))
            db.mealDao().insertAll(MealEntity(null, "Meal 5V13S", "£22",1))
            db.mealDao().insertAll(MealEntity(null, "Meal 4A4LO", "£14",1))
            db.mealDao().insertAll(MealEntity(null, "Meal G82BV", "£21",1))
            db.mealDao().insertAll(MealEntity(null, "Meal FQXAT", "£11",1))
            db.mealDao().insertAll(MealEntity(null, "Meal 60ZYG", "£16",1))
            db.mealDao().insertAll(MealEntity(null, "Meal 3NADK", "£15",1))
            db.mealDao().insertAll(MealEntity(null, "Meal 7EFL4", "£5",1))
            db.mealDao().insertAll(MealEntity(null, "Meal 4W7QX", "£22",1))
            db.mealDao().insertAll(MealEntity(null, "Meal 40LSQ", "£16",1))
            db.mealDao().insertAll(MealEntity(null, "Meal UFLN2", "£17",1))

            db.mealDao().insertAll(MealEntity(null, "Meal W4D74", "£19",2))
            db.mealDao().insertAll(MealEntity(null, "Meal OY8B4", "£15",2))
            db.mealDao().insertAll(MealEntity(null, "Meal 6TS0J", "£22",2))
            db.mealDao().insertAll(MealEntity(null, "Meal BEJ7X", "£19",2))
            db.mealDao().insertAll(MealEntity(null, "Meal 4YSLF", "£15",2))
            db.mealDao().insertAll(MealEntity(null, "Meal RAVLL", "£21",2))
            db.mealDao().insertAll(MealEntity(null, "Meal DMN1B", "£3",2))
            db.mealDao().insertAll(MealEntity(null, "Meal OOTDG", "£24",2))
            db.mealDao().insertAll(MealEntity(null, "Meal RRBYC", "£19",2))
            db.mealDao().insertAll(MealEntity(null, "Meal LZY0F", "£8",2))
            db.mealDao().insertAll(MealEntity(null, "Meal HCHW0", "£3",2))
            db.mealDao().insertAll(MealEntity(null, "Meal NWWS8", "£16",2))
            db.mealDao().insertAll(MealEntity(null, "Meal Z63ZX", "£12",2))
            db.mealDao().insertAll(MealEntity(null, "Meal 88TYS", "£10",2))

            db.mealDao().insertAll(MealEntity(null, "Meal 3K00X", "£18",3))
            db.mealDao().insertAll(MealEntity(null, "Meal PWCBN", "£22",3))
            db.mealDao().insertAll(MealEntity(null, "Meal JMFJN", "£16",3))
            db.mealDao().insertAll(MealEntity(null, "Meal P14QE", "£19",3))
            db.mealDao().insertAll(MealEntity(null, "Meal P0EJJ", "£6",3))
            db.mealDao().insertAll(MealEntity(null, "Meal L8DVT", "£2",3))
            db.mealDao().insertAll(MealEntity(null, "Meal DOW2C", "£4",3))
            db.mealDao().insertAll(MealEntity(null, "Meal 91DYW", "£21",3))
            db.mealDao().insertAll(MealEntity(null, "Meal BS5RO", "£20",3))
            db.mealDao().insertAll(MealEntity(null, "Meal B70UI", "£9",3))
            db.mealDao().insertAll(MealEntity(null, "Meal VTO86", "£18",3))
            db.mealDao().insertAll(MealEntity(null, "Meal IZT34", "£4",3))
            db.mealDao().insertAll(MealEntity(null, "Meal UV750", "£21",3))
            db.mealDao().insertAll(MealEntity(null, "Meal YAAY7", "£10",3))

            db.mealDao().insertAll(MealEntity(null, "Meal H08J2", "£9",4))
            db.mealDao().insertAll(MealEntity(null, "Meal JH8TR", "£21",4))
            db.mealDao().insertAll(MealEntity(null, "Meal UEPAH", "£13",4))
            db.mealDao().insertAll(MealEntity(null, "Meal 94FYG", "£12",4))
            db.mealDao().insertAll(MealEntity(null, "Meal 42K07", "£2",4))
            db.mealDao().insertAll(MealEntity(null, "Meal XWQ0E", "£9",4))
            db.mealDao().insertAll(MealEntity(null, "Meal IEOQP", "£2",4))
            db.mealDao().insertAll(MealEntity(null, "Meal AHLTH", "£5",4))
            db.mealDao().insertAll(MealEntity(null, "Meal HKQET", "£3",4))
            db.mealDao().insertAll(MealEntity(null, "Meal E6LBI", "£16",4))
            db.mealDao().insertAll(MealEntity(null, "Meal HVYGT", "£10",4))
            db.mealDao().insertAll(MealEntity(null, "Meal GHV0D", "£22",4))
            db.mealDao().insertAll(MealEntity(null, "Meal HPKY4", "£10",4))
            db.mealDao().insertAll(MealEntity(null, "Meal 69B21", "£6",4))

            db.mealDao().insertAll(MealEntity(null, "Meal MMS5I", "£2",5))
            db.mealDao().insertAll(MealEntity(null, "Meal Y2LX7", "£22",5))
            db.mealDao().insertAll(MealEntity(null, "Meal 7XP4M", "£13",5))
            db.mealDao().insertAll(MealEntity(null, "Meal 0U0Z6", "£25",5))
            db.mealDao().insertAll(MealEntity(null, "Meal 8GOVN", "£4",5))
            db.mealDao().insertAll(MealEntity(null, "Meal 1WM9U", "£2",5))
            db.mealDao().insertAll(MealEntity(null, "Meal YIYCU", "£14",5))
            db.mealDao().insertAll(MealEntity(null, "Meal W0L09", "£19",5))
            db.mealDao().insertAll(MealEntity(null, "Meal 43JBD", "£14",5))
            db.mealDao().insertAll(MealEntity(null, "Meal EWKUO", "£24",5))
            db.mealDao().insertAll(MealEntity(null, "Meal 1AT33", "£21",5))
            db.mealDao().insertAll(MealEntity(null, "Meal K2KGA", "£23",5))
            db.mealDao().insertAll(MealEntity(null, "Meal L2HN2", "£12",5))
            db.mealDao().insertAll(MealEntity(null, "Meal MOSB2", "£2",5))

            db.mealDao().insertAll(MealEntity(null, "Meal F9R10", "£6",6))
            db.mealDao().insertAll(MealEntity(null, "Meal U9CRR", "£6",6))
            db.mealDao().insertAll(MealEntity(null, "Meal Y96QP", "£11",6))
            db.mealDao().insertAll(MealEntity(null, "Meal J36PR", "£24",6))
            db.mealDao().insertAll(MealEntity(null, "Meal MB372", "£19",6))
            db.mealDao().insertAll(MealEntity(null, "Meal DZBI9", "£15",6))
            db.mealDao().insertAll(MealEntity(null, "Meal 9P942", "£14",6))
            db.mealDao().insertAll(MealEntity(null, "Meal ZVXWQ", "£17",6))
            db.mealDao().insertAll(MealEntity(null, "Meal AF7CV", "£24",6))
            db.mealDao().insertAll(MealEntity(null, "Meal CAZ1V", "£24",6))
            db.mealDao().insertAll(MealEntity(null, "Meal J0UEC", "£21",6))
            db.mealDao().insertAll(MealEntity(null, "Meal CB4W0", "£8",6))
            db.mealDao().insertAll(MealEntity(null, "Meal B2MLW", "£19",6))
            db.mealDao().insertAll(MealEntity(null, "Meal X8NLN", "£17",6))

            db.mealDao().insertAll(MealEntity(null, "Meal 6CVTB", "£23",7))
            db.mealDao().insertAll(MealEntity(null, "Meal MXKNM", "£5",7))
            db.mealDao().insertAll(MealEntity(null, "Meal 9JE4D", "£9",7))
            db.mealDao().insertAll(MealEntity(null, "Meal SKEXJ", "£20",7))
            db.mealDao().insertAll(MealEntity(null, "Meal DM9DY", "£13",7))
            db.mealDao().insertAll(MealEntity(null, "Meal JZ92I", "£11",7))
            db.mealDao().insertAll(MealEntity(null, "Meal AWDZD", "£20",7))
            db.mealDao().insertAll(MealEntity(null, "Meal B4JWK", "£23",7))
            db.mealDao().insertAll(MealEntity(null, "Meal TZ4GE", "£17",7))
            db.mealDao().insertAll(MealEntity(null, "Meal 9XT4J", "£17",7))
            db.mealDao().insertAll(MealEntity(null, "Meal EHY3H", "£15",7))
            db.mealDao().insertAll(MealEntity(null, "Meal WNNJC", "£10",7))
            db.mealDao().insertAll(MealEntity(null, "Meal 7GIEN", "£7",7))
            db.mealDao().insertAll(MealEntity(null, "Meal FDS0N", "£4",7))

            db.mealDao().insertAll(MealEntity(null, "Meal P9HMW", "£10",8))
            db.mealDao().insertAll(MealEntity(null, "Meal 0RQ5H", "£7",8))
            db.mealDao().insertAll(MealEntity(null, "Meal X0DIC", "£20",8))
            db.mealDao().insertAll(MealEntity(null, "Meal 6BMVU", "£16",8))
            db.mealDao().insertAll(MealEntity(null, "Meal JNNG2", "£23",8))
            db.mealDao().insertAll(MealEntity(null, "Meal EY378", "£7",8))
            db.mealDao().insertAll(MealEntity(null, "Meal SFUFI", "£13",8))
            db.mealDao().insertAll(MealEntity(null, "Meal 0B4CS", "£13",8))
            db.mealDao().insertAll(MealEntity(null, "Meal 2XTMO", "£8",8))
            db.mealDao().insertAll(MealEntity(null, "Meal MKCS5", "£25",8))
            db.mealDao().insertAll(MealEntity(null, "Meal 1K7IK", "£13",8))
            db.mealDao().insertAll(MealEntity(null, "Meal 0Y6HJ", "£19",8))
            db.mealDao().insertAll(MealEntity(null, "Meal HMYTD", "£16",8))
            db.mealDao().insertAll(MealEntity(null, "Meal 8H9F4", "£3",8))

            db.mealDao().insertAll(MealEntity(null, "Meal KFLPD", "£8",9))
            db.mealDao().insertAll(MealEntity(null, "Meal Q6GA9", "£12",9))
            db.mealDao().insertAll(MealEntity(null, "Meal CR5TW", "£4",9))
            db.mealDao().insertAll(MealEntity(null, "Meal PVEJI", "£11",9))
            db.mealDao().insertAll(MealEntity(null, "Meal BG2HY", "£24",9))
            db.mealDao().insertAll(MealEntity(null, "Meal QGJYV", "£20",9))
            db.mealDao().insertAll(MealEntity(null, "Meal ESYEW", "£24",9))
            db.mealDao().insertAll(MealEntity(null, "Meal KUBEJ", "£17",9))
            db.mealDao().insertAll(MealEntity(null, "Meal G7TP7", "£13",9))
            db.mealDao().insertAll(MealEntity(null, "Meal R76S9", "£15",9))
            db.mealDao().insertAll(MealEntity(null, "Meal HO5WF", "£25",9))
            db.mealDao().insertAll(MealEntity(null, "Meal 1K5M5", "£22",9))
            db.mealDao().insertAll(MealEntity(null, "Meal CSGI9", "£12",9))
            db.mealDao().insertAll(MealEntity(null, "Meal NELDB", "£16",9))

            db.mealDao().insertAll(MealEntity(null, "Meal PACL9", "£2",10))
            db.mealDao().insertAll(MealEntity(null, "Meal N2QKM", "£21",10))
            db.mealDao().insertAll(MealEntity(null, "Meal IRXQ6", "£14",10))
            db.mealDao().insertAll(MealEntity(null, "Meal DVH7P", "£8",10))
            db.mealDao().insertAll(MealEntity(null, "Meal MGX4M", "£17",10))
            db.mealDao().insertAll(MealEntity(null, "Meal ZMSG6", "£16",10))
            db.mealDao().insertAll(MealEntity(null, "Meal QZGZK", "£11",10))
            db.mealDao().insertAll(MealEntity(null, "Meal S9WFD", "£3",10))
            db.mealDao().insertAll(MealEntity(null, "Meal C3KR8", "£10",10))
            db.mealDao().insertAll(MealEntity(null, "Meal FVXE5", "£18",10))
            db.mealDao().insertAll(MealEntity(null, "Meal C625L", "£14",10))
            db.mealDao().insertAll(MealEntity(null, "Meal MU14F", "£18",10))
            db.mealDao().insertAll(MealEntity(null, "Meal Q12M4", "£13",10))
            db.mealDao().insertAll(MealEntity(null, "Meal 5GKWX", "£8",10))
        }
    }
}

// Hashing algorithm
fun md5(i : String): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1,md.digest(i.toByteArray())).toString(16).padStart(31,'0')
}