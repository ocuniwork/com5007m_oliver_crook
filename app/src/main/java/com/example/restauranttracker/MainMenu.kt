package com.example.restauranttracker

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainMenu : AppCompatActivity() {

    lateinit var toRestaurants : Button
    lateinit var toMeals : Button
    lateinit var toProfile : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mainmenu)

        // Setting up views
        toRestaurants = findViewById(R.id.buttonNavRestaurants)
        toMeals = findViewById(R.id.buttonNavMeals)
        toProfile = findViewById(R.id.buttonNavProfile)

        

        // If Restaurants button clicked, go to Restaurants Page
        toRestaurants.setOnClickListener {
            var intent = Intent(this@MainMenu,RestaurantList::class.java)
            startActivity(intent)
        }

        // If Meals button clicked, go to Meals page
        toMeals.setOnClickListener {
            var intent = Intent(this@MainMenu,Meals::class.java)
            startActivity(intent)
        }

        // If Profile button clicked, go to Profile page
        toProfile.setOnClickListener {
            var intent = Intent(this@MainMenu,Profile::class.java)
            startActivity(intent)
        }

    }
}