package com.example.restauranttracker

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ScrollView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.restauranttracker.database.AppDatabase
import com.example.restauranttracker.database.OrderEntity

class RestaurantDetail : AppCompatActivity() {

    lateinit var scroll : ScrollView
    lateinit var name : TextView
    lateinit var address : TextView
    lateinit var desc : TextView
    lateinit var menu : TextView
    lateinit var flavour : TextView
    lateinit var price : TextView

    lateinit var navAddReview : Button
    lateinit var recyclerReviews : RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurantdetail)

        // Setting up views
        flavour = findViewById(R.id.textAverageFlavour)
        price = findViewById(R.id.textAveragePrice)
        menu = findViewById(R.id.textMenu)
        scroll = findViewById(R.id.ScrollView)
        name = findViewById(R.id.textRestaurantName)
        address = findViewById(R.id.textAddressDetail)
        desc = findViewById(R.id.textDescriptionDetail)
        menu = findViewById(R.id.textMenu)
        navAddReview = findViewById(R.id.addReview)
        recyclerReviews = findViewById(R.id.recyclerReviews)

        // Get database
        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "restaurantdatabase"
        ).allowMainThreadQueries().build()

        // Get full orderlist and add orders whose restaurant matches Session's restaurant to ordersofrestaurant
        var orderlist = db.orderDao().getAll()
        var ordersofrestaurant = ArrayList<OrderEntity>()
        var restaurant = db.sessionDao().getSession()[0].restaurant
        orderlist.forEach {
            var x = it.orderMealID
            val mealrestaurantid = db.mealDao().getMealByID( x )[0].mealRestaurantID

            if(restaurant == mealrestaurantid)
                ordersofrestaurant.add(it)
        }

        // Flavour and Price scores start at 0.0
        // Each score is summed and then divided by number of orders to get the average
        var flavourscore : Double = 0.0
        var pricescore : Double = 0.0
        ordersofrestaurant.forEach{
            flavourscore += it.orderFlavourScore.toDouble()
            pricescore += it.orderPriceScore.toDouble()
        }
        flavour.text = "Flavour Score: ${String.format("%.2f",(flavourscore / ordersofrestaurant.size))}"
        price.text = "Price Score: ${String.format("%.2f",(pricescore / ordersofrestaurant.size))}"

        // Populating the menu
        // Get all meals where the meal's restaurant ID matches the Session restaurant
        var menulist = db.mealDao().getRestaurantMeals(restaurant!!)
        menu.text = "\n"
        menulist.forEach {
                menu.append(it.mealName.toString() + ", " + it.mealPrice.toString() + "\n")
        }

        // Start at the top and populate restaurant Name, Address, Description
        scroll.smoothScrollTo(0,0)
        name.text = intent.getStringExtra("name")
        address.text = intent.getStringExtra("address")
        desc.text = intent.getStringExtra("desc")

        // When the Add Review button is clicked, go to the Add Review page
        navAddReview.setOnClickListener {
            var intent = Intent(this@RestaurantDetail,AddReview::class.java)
            startActivity(intent)
        }

        // Set up and attach adapter to Orders recycler view using ordersofrestaurant
        recyclerReviews.layoutManager = LinearLayoutManager(this)
        var adapter = OrderAdapter(this, ordersofrestaurant)
        recyclerReviews.adapter = adapter


    }



}