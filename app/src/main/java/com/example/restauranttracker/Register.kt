package com.example.restauranttracker

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.room.Room
import com.example.restauranttracker.database.AppDatabase
import com.example.restauranttracker.database.UserEntity


class Register : AppCompatActivity() {

    lateinit var instructions : TextView
    lateinit var username : EditText
    lateinit var password : EditText
    lateinit var register : Button



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        // Get database
        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "restaurantdatabase"
        ).allowMainThreadQueries().build()

        // Set up views
        instructions = findViewById(R.id.textRegisterInstructions)
        username = findViewById(R.id.editTextUsernameRegister)
        password = findViewById(R.id.editTextPasswordRegister)
        register = findViewById(R.id.buttonRegister)

        // When the register button is clicked ...
        register.setOnClickListener {
            var u = username.text
            var p = password.text
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)

            // ... check username is the correct length ...
            if (u.length < 3 || u.length > 13){
                instructions.requestFocus()
                instructions.text = "Please make sure your username is between 4 and 12 characters"

            // ... then check if password is long enough ...
            } else if (p.length < 7) {
                instructions.text = "Please make sure your password is 8 characters long or more"

            // ... if checks pass then create a new entry in the database for the username and hashed password,
            // and then go to Login Page with user's information
            } else {
                instructions.text = "Registering!"
                db.userDao().insertAll(UserEntity(null, u.toString(), md5(p.toString())))
                val intent = Intent(this@Register, LoginPage::class.java)
                intent.putExtra("name", u.toString())
                intent.putExtra("pass", p.toString())
                startActivity(intent)

            }
        }

    }





}