package com.example.restauranttracker

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.room.Room
import com.example.restauranttracker.database.AppDatabase

class Profile : AppCompatActivity() {

    lateinit var logout : Button
    lateinit var textUsername : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        // Get db
        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "restaurantdatabase"
        ).allowMainThreadQueries().fallbackToDestructiveMigration().build()

        // Set up views
        logout = findViewById(R.id.buttonLogout)
        textUsername = findViewById(R.id.textUsername)

        // Set the Username text view to the current logged in user
        textUsername.text = db.sessionDao().getSession()[0].username.toString()

        // When Log Out button is clicked, set Session info to default, take user to Startup Screen
        logout.setOnClickListener {
            db.sessionDao().updateSession("nan", false)
            intent = Intent(this@Profile, MainActivity::class.java)
            startActivity(intent)
        }

    }
}