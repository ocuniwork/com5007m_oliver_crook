package com.example.restauranttracker

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Adapter
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.restauranttracker.database.AppDatabase
import com.example.restauranttracker.database.RestaurantEntity

class RestaurantList : AppCompatActivity() {

    lateinit var rv : RecyclerView
    lateinit var title : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurants)

        // Get database
        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "restaurantdatabase"
        ).allowMainThreadQueries().build()

        // Set up views
        title = findViewById(R.id.textRestaurantsTitle)
        rv = findViewById(R.id.rv)

        // Set up and attach adapter to Restaurants recycler view
        rv.layoutManager = LinearLayoutManager(this)
        var adapter = RestaurantAdapter(this, db.restaurantDao().getAll() as ArrayList<RestaurantEntity>)
        rv.adapter = adapter
    }
}