package com.example.restauranttracker

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.restauranttracker.database.AppDatabase
import com.example.restauranttracker.database.OrderEntity


class OrderAdapter(val context: Context, val items: ArrayList<OrderEntity>) :
    RecyclerView.Adapter<OrderAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        // Setting up views
        var card : CardView
        var mealname : TextView
        var username : TextView
        var restaurant : TextView
        var flavour : TextView
        var price : TextView
        var date : TextView

        init{
            card = view.findViewById(R.id.cvmeal)
            mealname = view.findViewById(R.id.textMealName)
            username = view.findViewById(R.id.textMealUsername)
            restaurant = view.findViewById(R.id.textMealRestaurant)
            flavour = view.findViewById(R.id.textMealFlavour)
            price = view.findViewById(R.id.textMealPrice)
            date = view.findViewById(R.id.textMealDate)

        }
    }

    // The viewholder will come from cardmeal.xml
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.cardmeal, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get item from array, and db
        val item = items.get(position)
        val db = Room.databaseBuilder(
            context,
            AppDatabase::class.java, "restaurantdatabase"
        ).allowMainThreadQueries().build()

        // Set variables to their correct values
        val mealname = db.mealDao().getMealByID(item.orderMealID)[0].mealName
        val username = db.userDao().getUserByID(item.orderUserID)[0].username

        val mealid = db.mealDao().getMealByID(item.orderMealID)[0].mealID
        val mealrestaurantid = db.mealDao().getMealByID(mealid!!)[0].mealRestaurantID// these are all 1 for some reason
        val restaurant = db.restaurantDao().getRestaurantByID(mealrestaurantid!!)[0]

        // Assign each view in the viewholder with the correct value
        viewHolder.mealname.text = mealname
        viewHolder.username.text = username
        viewHolder.restaurant.text = restaurant.restaurantName
        viewHolder.flavour.text = "Flavour: ${item.orderFlavourScore}"
        viewHolder.price.text = "Price: ${item.orderPriceScore}"
        viewHolder.date.text = item.orderDate

        // When a card is clicked, go to Restaurant Detail page with the necessary information in intent
        viewHolder.card.setOnClickListener {
            db.sessionDao().setRestaurant(restaurant.restaurantID!!)
            val intent = Intent(context, RestaurantDetail::class.java)
            intent.putExtra("name", restaurant.restaurantName)
            intent.putExtra("address", restaurant.restaurantAddress)
            intent.putExtra("desc", restaurant.restaurantDescription)
            intent.putExtra("id", restaurant.restaurantID)
            context.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
        return items.size
    }

}