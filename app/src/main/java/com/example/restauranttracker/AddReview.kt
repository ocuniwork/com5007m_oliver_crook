package com.example.restauranttracker

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.room.Room
import com.example.restauranttracker.database.AppDatabase
import com.example.restauranttracker.database.OrderEntity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.util.*
import kotlin.collections.ArrayList

class AddReview : AppCompatActivity() {

    lateinit var mealname : Spinner
    lateinit var date : EditText
    lateinit var flavour : SeekBar
    lateinit var price : SeekBar
    lateinit var addreview : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addreview)

        // Setting up views
        mealname = findViewById(R.id.spinnerMeal)
        date = findViewById(R.id.editTextDate)
        flavour = findViewById(R.id.seekBarFlavour)
        price = findViewById(R.id.seekBarPrice)
        addreview = findViewById(R.id.buttonAddReview)

        // Date picker dialog
        val c = Calendar.getInstance()
        val y = c.get(Calendar.YEAR)
        val m = c.get(Calendar.MONTH)
        val d = c.get(Calendar.DAY_OF_MONTH)

        date.setOnClickListener {
            val datepicker = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { datePicker, mYear, mMonth, mDay ->
                date.setText("" + mYear +"-"+ mMonth + "-" + mDay  )
            }, y, m, d)
            datepicker.show()
        }

        // Database
        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "restaurantdatabase"
        ).allowMainThreadQueries().build()

        var restaurantid = db.sessionDao().getSession()[0].restaurant
        var mealentities = db.mealDao().getRestaurantMeals(restaurantid!!)
        var meals = ArrayList<String>()
        mealentities.forEach {
            meals.add(it.mealName)
        }

        // Creating and attaching the adapter for the spinner
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, meals)
        mealname.adapter = adapter

        // Show a confirmation dialog when clicking Add Review
        addreview.setOnClickListener {
            showConfirmDialog(mealname.selectedItem.toString(), flavour.progress.toString(), price.progress.toString(), date.text.toString())
        }

    }

    // Function that creates and then shows a confirmation dialog box
    fun showConfirmDialog(meal : String, f : String, p : String, d : String){
        MaterialAlertDialogBuilder(this@AddReview)
            .setTitle("Confirm Review")
            .setMessage("Leave a review for ${meal} with Flavour Score: ${f}, and Price Score: ${p} at ${d}?")
            .setNegativeButton("No") {dialog, which ->

            }
            .setPositiveButton("Yes") {dialog, which ->
                val db = Room.databaseBuilder(
                    applicationContext,
                    AppDatabase::class.java, "restaurantdatabase"
                ).allowMainThreadQueries().build()

                var username = db.sessionDao().getSession()[0].username.toString()
                var userid = db.userDao().getUser(username)[0].userID
                var mealid = db.mealDao().getMeal(meal)[0].mealID

                // Add order information to DB
                db.orderDao().insertAll(OrderEntity(null, userid!!, mealid!!, f, p, d))

                intent = Intent(this@AddReview, MainMenu::class.java)
                startActivity(intent)
            }
            .show()
    }
}