package com.example.restauranttracker

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.restauranttracker.database.AppDatabase
import com.example.restauranttracker.database.RestaurantEntity

class RestaurantAdapter(val context: Context, val items: ArrayList<RestaurantEntity>) :
    RecyclerView.Adapter<RestaurantAdapter.ViewHolder>() {

    // Setting up views
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var name : TextView
        var address : TextView
        var description : TextView
        var card : CardView

        init{
            name = view.findViewById(R.id.textRestaurantName)
            address = view.findViewById(R.id.textAddress)
            description = view.findViewById(R.id.textDescription)
            card = view.findViewById(R.id.cv)
        }
    }

    // The viewholder will come from cardrestaurant.xml
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.cardrestaurant, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get item from the list
        val item = items.get(position)

        // Populate viewholder views with information from item
        viewHolder.name.text = item.restaurantName
        viewHolder.address.text = item.restaurantAddress
        viewHolder.description.text = item.restaurantDescription

        // If a card is clicked, set Session restaurant, load intent with info,
        // then take user to Restaurant Details page that will be populated with that info
        viewHolder.card.setOnClickListener {
            val db = Room.databaseBuilder(
                context,
                AppDatabase::class.java, "restaurantdatabase"
            ).allowMainThreadQueries().fallbackToDestructiveMigration().build()

            db.sessionDao().setRestaurant(item.restaurantID!!)

            val intent = Intent(context, RestaurantDetail::class.java)
            intent.putExtra("name", item.restaurantName)
            intent.putExtra("address", item.restaurantAddress)
            intent.putExtra("desc", item.restaurantDescription)
            intent.putExtra("id", item.restaurantID)

            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

}